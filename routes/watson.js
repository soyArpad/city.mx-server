//  Copyright © 2016 Arpad Larrinaga Aviles
//
//  City.mx is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  City.mx is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with City.mx.  If not, see <http://www.gnu.org/licenses/>.

var express = require('express');
var router = express.Router();

var watson = require('watson-developer-cloud');

var mongoose = require('mongoose');
var Answer = require('../models/answers.js');

var natural_language_classifier = watson.natural_language_classifier({

  username: 'b8e7ef31-865e-488f-a18d-681d6f872375',
  password: 'Wbha2kXMqjKz',
  version: 'v1'
});

router.post('/classify', function(req, res) {
  if (!req.body.text) {
    res.json({success: false, errorCode: 4});
  } else {
    natural_language_classifier.classify({
    text: req.body.text,
    classifier_id: '6688a2x37-nlc-256' },
    function(err, response) {
      if (err) {
        console.log('error:', err);
        res.json({success: false, errorCode: 4});
      }
      else {
        var topClass = response["top_class"];
        var classWatson = response["classes"][0];
        var retrieveResponse = false;
        if (classWatson["confidence"] > 0.70) {
          Answer.findOne({
            localID: topClass
          }, function(err, answer) {
            if (err)
              res.json({success: false, errorCode:4});
            
            res.json({success: true, message: answer.answer});
          });
        } else {
        res.json({success: true, message: "No contamos con dicha información, gracias por usar CITY MX cuando esta información esté disponible te notificaremos"});
        }
        console.log(JSON.stringify(response, null, 2));
      }
    });
  }
});

module.exports = router;
